﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    [SerializeField] float cycleLength = 5f;

    [SerializeField] Vector2 movementDirection;
    [SerializeField] Rigidbody2D rb;

    Vector2 startingPos;

	// Use this for initialization
	void Start () {
        startingPos = new Vector2(transform.position.x, transform.position.y);
	}
	
	// Update is called once per frame
	void Update () {
        float k = Oscillate();
        Vector2 offset = movementDirection * k;
        transform.position = startingPos + offset;
	}

    float Oscillate()
    {
        float interval = Time.timeSinceLevelLoad / cycleLength;
        float rawCosWave = -1 * Mathf.Cos(Mathf.PI * interval);
        return rawCosWave / 2f + 0.5f;
    }
}
